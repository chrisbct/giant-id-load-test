import http from 'k6/http'
import { check, sleep } from 'k6'
import { SignatureV4 } from 'https://jslib.k6.io/aws/0.8.1/signature.js'
import { AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_REGION } from '../config.js'

let signer

/**
 * In order to be able to sign an HTTP request's,
 * we need to instantiate a SignatureV4 object.
 */
export const getLambdaSigner = () => {
  let signer = new SignatureV4({
      service: 'lambda',
      region: AWS_REGION,
      credentials: {
          accessKeyId: AWS_ACCESS_KEY_ID,
          secretAccessKey: AWS_SECRET_ACCESS_KEY,
      },
  })
  return signer
}

export default function({ functionName, event }) {
  if (!signer) {
    signer = getLambdaSigner()
  }

  const marshalEvent = JSON.stringify(event)

  const signedRequest = signer.sign(
      /**
       * HTTP request description
       */
      {
          /**
           * The HTTP method we will use in the request.
           */
          method: 'POST',

          /**
           * The network protocol we will use to make the request.
           */
          protocol: 'https',

          /**
           * The hostname of the service we will be making the request to.
           */
          hostname: 'lambda.ap-northeast-1.amazonaws.com',

          /**
           * The path of the request.
           */
          path: `/2015-03-31/functions/${functionName}/invocations`,

          /**
           * The headers we will be sending in the request.
           */
          headers: {},

          /**
           * Whether the URI should be escaped or not.
           */
          uriEscapePath: false,

          /**
           * Whether or not the body's hash should be calculated and included
           * in the request.
           */
          applyChecksum: false,
          body: marshalEvent,
      },

      /**
       * (optional) Signature operation options allows to override the
       * SignatureV4's options in the context of this specific request.
       */
      {
          /**
           * The date and time to be used as signature metadata. This value should be
           * a Date object, a unix (epoch) timestamp, or a string that can be
           * understood by the JavaScript `Date` constructor.If not supplied, the
           * value returned by `new Date()` will be used.
           */
          signingDate: new Date(),

          /**
           * The service signing name. It will override the service name of the signer
           * in current invocation
           */
          signingService: 'lambda',

          /**
           * The region name to sign the request. It will override the signing region of the
           * signer in current invocation
           */
          signingRegion: 'ap-northeast-1',
      }
  )

  return http.post(
    signedRequest.url,
    marshalEvent,
    {
      headers: signedRequest.headers
    },
  );
}
