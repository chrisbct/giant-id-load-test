import http from 'k6/http'
import { URL, URLSearchParams } from 'https://jslib.k6.io/url/1.0.0/index.js'
import urlencode from 'https://jslib.k6.io/form-urlencoded/3.0.0/index.js'
import { check, sleep } from 'k6';

import {
    BASE_URL,
    CALLBACK_URI,
    CLIENT_ID,
    COGNITO_ENDPOINT,
    USERNAME,
    PASSWORD,
    API_KEY,
    TEST_REFRESH_TOKEN_USERNAME,
    TEST_REFRESH_TOKEN_PASSWORD,
} from '../config.js'

export function getToken() {
    const body = {
        username: USERNAME,
        password: PASSWORD,
    }

    const param = {
        headers: {
            'x-api-key': API_KEY,
        },
    }

    let response = http.post(
        `${BASE_URL}/oauth/login?client_id=${CLIENT_ID}&redirect_uri=${CALLBACK_URI}`,
        JSON.stringify(body),
        param,
    )

    const status = check(response, { 'status was 200': (r) => r.status == 200 });

    if (!status) {
        console.error('err/giant-id-v2-OauthLoginFunction', response)
        throw 'err/oauth-login/status'
    }

    let responseUrl = response.json('location')
    const url = new URL(responseUrl).searchParams

    const code = url.get('code')

    return exchangeAuthCode(code)
}

const exchangeAuthCode = (code) => {
    const body = urlencode({
        grant_type: 'authorization_code',
        code: code,
        client_id: CLIENT_ID,
        redirect_uri: CALLBACK_URI,
    })

    console.log(body)

    const params = {
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    }

    const resp = http.post(`${COGNITO_ENDPOINT}/oauth2/token`, body, params)
    const token = resp.json('access_token')
    return token
}

export function getAllToken() {
    const body = {
        username: TEST_REFRESH_TOKEN_USERNAME,
        password: TEST_REFRESH_TOKEN_PASSWORD,
    }

    const param = {
        headers: {
            'x-api-key': API_KEY,
        },
    }

    let response = http.post(
        `${BASE_URL}/oauth/login?client_id=${CLIENT_ID}&redirect_uri=${CALLBACK_URI}`,
        JSON.stringify(body),
        param,
    )

    let responseUrl = response.json('location')
    const url = new URL(responseUrl).searchParams

    const code = url.get('code')

    const body2 = urlencode({
        grant_type: 'authorization_code',
        code: code,
        client_id: CLIENT_ID,
        redirect_uri: CALLBACK_URI,
    })

    const params = {
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    }

    const resp = http.post(`${COGNITO_ENDPOINT}/oauth2/token`, body2, params)
    return resp
}
