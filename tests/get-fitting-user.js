import http from 'k6/http'
import { check, sleep } from 'k6'

import { API_ENDPOINT, API_KEY } from '../config.js'

export default function () {
    const email = 'abc@gmail.com'
    let res
    res = http.get(`${API_ENDPOINT}/users/service/fitting?email=${email}`, {
        headers: {
            'x-api-key': API_KEY,
        },
    })
    check(res, { 'status was 200': (r) => r.status == 200 })
}
