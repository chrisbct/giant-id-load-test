import { URL } from 'https://jslib.k6.io/url/1.0.0/index.js'
import http from 'k6/http'
import { check } from 'k6'

import { API_ENDPOINT, CLIENT_ID, CALLBACK_URI, API_KEY } from '../config.js'

export default function () {
    const url = new URL(`${API_ENDPOINT}/callback`)

    url.searchParams.append('clientId', CLIENT_ID)
    url.searchParams.append('callbackUri', CALLBACK_URI)

    const res = http.get(url.toString(), {
        headers: {
            'x-api-key': API_KEY
        },
    })
    check(res, { 'status was 200': (r) => r.status == 200 })

    const status = check(res, { 'status was 200': (r) => r.status == 200 });
    if (!status) {
        console.warn(res)
    }
}
