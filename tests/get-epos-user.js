import http from 'k6/http'
import { check, sleep } from 'k6'

import { API_ENDPOINT, API_KEY } from '../config.js'

export default function () {
    const phone = encodeURIComponent('+886222862369')
    const gid = '94bf-659b-658d-46b4'
    let res
    res = http.get(`${API_ENDPOINT}/users/service/pos?phone=${phone}&gid=${gid}`, {
        headers: {
            'x-api-key': API_KEY,
        },
    })
    check(res, { 'status was 200': (r) => r.status == 200 })
}
