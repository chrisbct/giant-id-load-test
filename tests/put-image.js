import http from 'k6/http'
import { check, sleep } from 'k6'

import { getToken } from '../shared/index.js'
import { API_ENDPOINT } from '../config.js'

let ACCESS_TOKEN = ''
const img = open('./profile.png', 'b');

export default function () {
    if (!ACCESS_TOKEN) {
        ACCESS_TOKEN = getToken()
        console.log(ACCESS_TOKEN)
    }

    const res = http.post(`${API_ENDPOINT}/user/picture`, img, {
        headers: {
            'content-disposition': 'attachment;filename="profile.png"',
            Authorization: `Bearer ${ACCESS_TOKEN}`,
            'Content-Type': 'image/png',
        },
    })

    const status = check(res, { 'status was 200': (r) => r.status == 200 });
    if (!status) {
        console.warn(res)
    }
}
