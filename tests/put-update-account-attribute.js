import http from 'k6/http'
import { check, sleep } from 'k6'

import { getToken } from '../shared/index.js'
import { API_ENDPOINT } from '../config.js'

let ACCESS_TOKEN = ''

export default function () {
    if (!ACCESS_TOKEN) {
        ACCESS_TOKEN = getToken()
    }

    const body = {
        email: 'chris.chang@bahwancybertek.com',
    }

    const res = http.put(`${API_ENDPOINT}/account`, JSON.stringify(body), {
        headers: {
            Authorization: `Bearer ${ACCESS_TOKEN}`,
        },
    })
    check(res, { 'status was 200': (r) => r.status == 200 })
}
