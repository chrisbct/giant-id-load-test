import http from 'k6/http'
import { check, sleep } from 'k6'

import { getToken } from '../shared/index.js'
import { API_ENDPOINT } from '../config.js'

let ACCESS_TOKEN = ''

export default function () {
    if (!ACCESS_TOKEN) {
        ACCESS_TOKEN = getToken()
    }

    const res = http.get(`${API_ENDPOINT}/token`, {
        headers: {
            Authorization: `Bearer ${ACCESS_TOKEN}`,
        },
    })
    check(res, { 'status was 200': (r) => r.status == 200 })

    const status = check(res, { 'status was 200': (r) => r.status == 200 });
    if (!status) {
        console.warn(res)
    }
}
