import http from 'k6/http'
import { check, sleep } from 'k6'

import { getToken } from '../shared/index.js'
import { API_ENDPOINT } from '../config.js'

let ACCESS_TOKEN = ''

export default function () {
    if (!ACCESS_TOKEN) {
        ACCESS_TOKEN = getToken()
    }

    const body = {
        sub: '3c33a26b-7bcd-407f-bd76-a710712daeba',
        provider: 'Google',
    }

    const res = http.post(`${API_ENDPOINT}/account`, JSON.stringify(body), {
        headers: {
            Authorization: `Bearer ${ACCESS_TOKEN}`,
        },
    })
    check(res, { 'status was 400': (r) => r.status == 400 })
}
