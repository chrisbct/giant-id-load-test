import { SharedArray } from 'k6/data';
import { check } from 'k6'
import exec from "k6/execution";

import papaparse from 'https://jslib.k6.io/papaparse/5.1.1/index.js';

import invoke from '../shared/invoke.js'

const csvData = new SharedArray('another data name', function () {
    // Load CSV file and parse it using Papa Parse
    return papaparse.parse(open('../users.csv'), { header: true }).data;
});

export default function () {
  const idx = exec.scenario.iterationInTest % csvData.length
  const sub = csvData[idx].sub

  const invokeEvent = generatePayload(sub)
  const res = invoke({
    functionName: "dev-giant-id-v2-DeleteUserFunction",
    event: invokeEvent,
  });

  const status = check(res, {
    'status was 200': (r) => r.status == 200,
    'response': (r) => {
      // console.log(r)
      const lambdaInvokeResult = JSON.parse(r.body)
      return lambdaInvokeResult.statusCode == 200
    }
  });
  if (!status) {
    // status not ok
    console.warn(res)
  }
}

const generatePayload = (sub) => {
  return {
    requestContext: {
      authorizer: {
        claims: {
          sub: sub
        }
      }
    }
  }
}
