import http from 'k6/http';
import { check, sleep } from 'k6';

import { getToken } from '../shared/index.js';
import { API_ENDPOINT, API_KEY } from '../config.js';

let ACCESS_TOKEN = ''

export default function () {
    if (!ACCESS_TOKEN) {
        ACCESS_TOKEN = getToken()
    }

    const body = {
        emailList: [
            "abc123@gmail.com"
        ]
    }

    const res = http.post(`${API_ENDPOINT}/users`, JSON.stringify(body), {
        headers: {
            'x-api-key': `${API_KEY}`
        }
    });

    console.log(JSON.stringify(res, null, 2))
    check(res, { 'status was 200': (r) => r.status == 200 });
}