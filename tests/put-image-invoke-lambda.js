import { SharedArray } from 'k6/data';
import { check,  } from 'k6'
import encoding from 'k6/encoding';

import papaparse from 'https://jslib.k6.io/papaparse/5.1.1/index.js';

import invoke from '../shared/invoke.js'

const csvData = new SharedArray('another data name', function () {
    // Load CSV file and parse it using Papa Parse
    return papaparse.parse(open('../users.csv'), { header: true, skipEmptyLines: true }).data;
});

const img = open('./profile.png', 'b');

export default function () {
  const randomIdx = Math.floor(Math.random() * csvData.length)
  const sub = csvData[randomIdx].sub

  const invokeEvent = encoding.b64encode(img)
  const payload = generatePayload(sub, invokeEvent)

  const res = invoke({
    functionName: "dev-giant-id-v2-AddUserPicture",
    event: payload,
  });

  const status = check(res, {
    'status was 200': (r) => r.status == 200,
    'response': (r) => {
      const lambdaInvokeResult = JSON.parse(r.body)
      return lambdaInvokeResult.statusCode == 200
    }
  });
  if (!status) {
    // status not ok
    console.warn(res)
  }
}

const generatePayload = (sub, body) => {
  return {
    requestContext: {
      authorizer: {
        claims: {
          sub: sub,
          scope: "info/all.write aws.cognito.signin.user.admin info/all.read info/basic.write phone info/basic.read openid profile email"
        },
      },
    },
    isBase64Encoded: true,
    headers: {
      'content-disposition': 'attachment;filename="cat.jpg"',
    },
    body: body
  };
}
