import { check } from 'k6'

import faker from 'https://cdnjs.cloudflare.com/ajax/libs/Faker/3.1.0/faker.min.js';
import { uuidv4 } from 'https://jslib.k6.io/k6-utils/1.4.0/index.js';

import { AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_REGION } from '../config.js'
import invoke from '../shared/invoke.js'

export function setup() {
  console.log('sub,firstName,lastName')
}

export default function () {
  const invokeEvent = generatePayload()
  const res = invoke({
    functionName: "dev-giant-id-v2-PostConfirmTriggerFunction",
    event: invokeEvent,
  });

  const status = check(res, {
    'status was 200': (r) => r.status == 200,
    'response': (r) => {
      const body = JSON.parse(r.body)
      if (body.errorType) {
        console.warn(r)
        return false;
      }
      const { request: { sub, userAttributes: { family_name, given_name } } } = body;
      console.log(`${sub},${given_name},${family_name}`)
      return true
    }
  });
  if (!status) {
    // status not ok
    console.warn(res)
  }
}

const generatePayload = () => {
  return {
    triggerSource: "PostConfirmation_ConfirmSignUp",
    request: {
      sub: `ut-sub-${uuidv4()}`,
      username: `ut-username-${uuidv4()}`,
      userAttributes: {
        family_name: faker.name.firstName(),
        given_name: faker.name.lastName(),
      }
    }
  }
}
