import http from 'k6/http';
import { check, sleep } from 'k6';

export default function () {
    const res = http.get(`https://k3cu8gwi79.execute-api.ap-northeast-1.amazonaws.com/lab/ddb-get-item`);

    const status = check(res, { 'status was 200': (r) => r.status == 200 });
    console.log(res)

    if (!status) {
        console.warn(res)
    }
}