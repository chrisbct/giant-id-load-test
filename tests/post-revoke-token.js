import http from 'k6/http'
import { check, sleep } from 'k6'

import { getAllToken } from '../shared/index.js'
import { API_ENDPOINT } from '../config.js'

let ALL_TOKEN = ''

export default function () {
    if (!ALL_TOKEN) {
        ALL_TOKEN = getAllToken()
    }

    const accessToken = ALL_TOKEN.json('access_token')
    const refreshToken = ALL_TOKEN.json('refresh_token')

    const body = {
        refreshToken: refreshToken,
    }

    const res = http.post(`${API_ENDPOINT}/token`, JSON.stringify(body), {
        headers: {
            Authorization: `Bearer ${accessToken}`,
        },
    })
    check(res, { 'status was 200': (r) => r.status == 200 })
}
